# Zorg

Zorg is a small bot which can follosw RSS feed and post the new entries to your MastodonAPI-compatible pod. 
I tested it with Pleroma so far, and it works.

To install, just clone this repository like

``` 
git clone https://git.keinpfusch.net/git/Loweel/zorg.git

go build

``` 



just change the values to match your pod, and it will print a new ClientID and ClientSecret.

Once you have both, you can just feed the zorg configuration file, which is a JSON, and change his name as zorg.conf
Just leave "default" on your ClientID and secret.

```
{  
    "ZorgServer":       "https://example-pleroma.net",
	"ZorgClientID":     "default",
	"ZorgClientSecret": "default",
	"ZorgUname":        "johndoe",
	"ZorgPass" :        "lalalalalalalala",
	"ZorgInterval":     7200
    }

```

Please notice that ZorgUname and ZorgPass are credentials for the user you want the RSS feeds to be published with.

ZorgInterval is the interval of polling all the feeds you like.

The list of RSS feeds must be inside a file named feeds.conf, and it's just a one-line-per-url list of RSS feed urls, like

``` 
https://www.youtube.com/feeds/videos.xml?channel_id=UCDmCBKaKOtOrEqgsL4-3C8Q
https://www.youtube.com/feeds/videos.xml?channel_id=UCYcXk-yEg9LgyAPm7mpIs-g
https://www.youtube.com/feeds/videos.xml?channel_id=UCNvsIonJdJ5E4EXMa65VYpA
https://www.youtube.com/feeds/videos.xml?channel_id=UCeYbbVx7CQn6tbkkmpPldTg
https://www.youtube.com/feeds/videos.xml?channel_id=UCvKejuca1oOhFRXpZpSn3ew
https://blog.soykaf.com/post/index.xml
https://blog.golang.org/feed.atom
https://writings.stephenwolfram.com/feed/
https://www.youtube.com/feeds/videos.xml?channel_id=UCwUizOU8pPWXdXNniXypQEQ
https://www.youtube.com/feeds/videos.xml?channel_id=UCuCYsYBaq3j0gM4wWo82LkQ

```

Have fun. 

TODO: 

- to have more than one item per feed polling. 



