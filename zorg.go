package main

import (
	"os"
	"os/signal"
	"syscall"
)

func main() {

	// Just a nice way to wait until the Operating system sends a kill signal.
	// select{} was just horrible.
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	<-c
	os.Exit(0)

}
