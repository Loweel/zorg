module zorg

go 1.13

require (
	github.com/grokify/html-strip-tags-go v0.0.1
	github.com/mattn/go-mastodon v0.0.4
	github.com/mmcdole/gofeed v1.0.0
)
