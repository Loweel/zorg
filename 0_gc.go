package main

import (
	"log"
	"runtime"
	"time"
)

func init() {

	log.Println("Garbage Collector Thread Starting")

	go memoryCleanerThread()

}

func memoryCleanerThread() {

	for {
		time.Sleep(Zint)
		log.Println("Time to clean memory...")
		runtime.GC()
		log.Println("Garbage Collection done.")
	}

}
