package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"time"

	"github.com/mattn/go-mastodon"
)

// ZorgConfig is the configuration of Zorg.
var ZorgConfig struct {
	ZorgServer       string `json:"ZorgServer"`
	ZorgClientID     string `json:"ZorgClientID"`
	ZorgClientSecret string `json:"ZorgClientSecret"`
	ZorgUname        string `json:"ZorgUname"`
	ZorgPass         string `json:"ZorgPass"`
	ZorgInterval     int    `json:"ZorgInterval"`
}

// Zint is the poll time
var Zint time.Duration

func init() {

	//reading json file
	file, err := ioutil.ReadFile("zorg.conf")

	if err != nil {
		log.Println("Cannot open config file", err.Error())
		os.Exit(1)
	}

	err = json.Unmarshal([]byte(file), &ZorgConfig)

	if err != nil {
		log.Println("Cannot marshal json: ", err.Error())
		os.Exit(1)
	}

	Zint = time.Duration(time.Duration(ZorgConfig.ZorgInterval) * time.Second)

	ZorgCreateToken()

	log.Println("Inizialized ZORG")

}

func ZorgCreateToken() {
	app, err := mastodon.RegisterApp(context.Background(), &mastodon.AppConfig{
		Server:     ZorgConfig.ZorgServer,
		ClientName: "Zorg",
		Scopes:     "read write follow",
		Website:    ZorgConfig.ZorgServer,
	})
	if err != nil {
		log.Fatal(err)
	}

	ZorgConfig.ZorgClientID = app.ClientID
	ZorgConfig.ZorgClientSecret = app.ClientSecret

	fmt.Printf("Zorg new client-id    : %s\n", ZorgConfig.ZorgClientID)
	fmt.Printf("Zorg new client-secret: %s\n", ZorgConfig.ZorgClientSecret)

}
