package main

import (
	"context"
	"log"

	"github.com/mattn/go-mastodon"
)

func postOnMastodon(message, title string) {
	c := mastodon.NewClient(&mastodon.Config{

		Server:       ZorgConfig.ZorgServer,
		ClientID:     ZorgConfig.ZorgClientID,
		ClientSecret: ZorgConfig.ZorgClientSecret,
	})
	err := c.Authenticate(context.Background(), ZorgConfig.ZorgUname, ZorgConfig.ZorgPass)
	if err != nil {
		log.Println(err)
	} else {
		log.Println("Authenticated to the server: ", ZorgConfig.ZorgServer)

	}

	c.PostStatus(context.Background(), &mastodon.Toot{
		Status:      message,
		SpoilerText: title,
	})

}
